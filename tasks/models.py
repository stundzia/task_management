from __future__ import unicode_literals

from django.db import models
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User


class Task(models.Model):
    """Task model."""

    states = [
        ('new', 'To Do'),
        ('open', 'In Progress'),
        ('done', 'Done'),
    ]
    title = models.CharField(max_length=50)
    description = models.TextField()
    state = models.CharField(max_length=20, choices=states, default='new')
    # user_id = models.ForeignKey(to=User)

    def __unicode__(self):
        if self.title:
            return self.title
        elif len(self.description) > 35:
            return self.description[:33] + "..."
        return self.description

    def get_absolute_url(self):
        return reverse('tasks:form', kwargs={'pk': self.pk})
