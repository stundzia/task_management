from datetime import date

from django import forms
from django.forms import ModelForm
from .models import Task


class TaskForm(ModelForm):
    created_at = forms.DateTimeField(initial=date.today())

    class Meta:
        model = Task
        fields = ['title', 'description', 'state']
