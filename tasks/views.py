from django.contrib.auth.decorators import user_passes_test
from .models import Task
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, request, HttpResponseRedirect
from django.core.urlresolvers import reverse
from .forms import TaskForm
from .serializers import TaskSerializer
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status


def check_auth(user):
    return user.is_authenticated


@user_passes_test(check_auth, login_url='login')
def index(request):
    return render(request, 'tasks/index.html')


def submit_task(request):
    if request.POST:
        form = TaskForm(request.POST)
        if form.is_valid():
            invoice = form.save()
            invoice.save()
            return HttpResponseRedirect(reverse('task_list'))
    else:
        form = TaskForm()
    return render(request, 'tasks/task_create.html', {'form': form})


# List all tasks or create a new one
class TaskList(APIView):

    def get(self, request):
        tasks = Task.objects.all()
        serializer = TaskSerializer(tasks, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = TaskSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
