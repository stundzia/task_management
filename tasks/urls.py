from django.conf.urls import url
from django.views.generic import ListView, DetailView

from . import views
from .models import Task

urlpatterns = [
    # /index
    url(r'^$', views.index, name='index'),
    url(
        r'^tasks/(?P<pk>\d+)$',
        DetailView.as_view(
            model=Task,
            template_name="tasks/task_form.html"
        ), name='task_form'
    ),
    url(
        r'^tasks/list$',
        ListView.as_view(
            queryset=Task.objects.all().order_by('title')[:50],
            template_name="tasks/task_list.html"
        ), name='task_list'
    ),
    url(
        r'^tasks/new/post$',
        views.submit_task, name='submit_task'
    ),
]
